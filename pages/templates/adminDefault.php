<?php

use Src\App\App;
use Src\Core\Html\HTML;
$data = json_encode(App::getInstance()->getTable('statistic')->getYearSalesStatistic(2018));
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin - Charts</title>

    <!-- Bootstrap core CSS-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.css">
    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">


    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</head>

<body id="page-top"  class="<?= ($_SESSION['sidebar'])?'sidebar-toggled':'';?>">

<?= HTML::getAdminNav(); ?>
<div id="wrapper">

    <!-- Sidebar -->
    <?= HTML::getSideBar($page);?>

	<!-- .content-wrapper -->
	<div id="content-wrapper">
		<!-- .container-fluid -->
		<div class="container-fluid">

	    <?= $content?>
	    <!-- /.content-wrapper -->
		</div>
		<!-- /.container-fluid -->

<!--		 Sticky Footer -->
		<footer class="sticky-footer">
			<div class="container my-auto">
				<div class="copyright text-center my-auto">
					<span>Copyright © Medovinka 2018</span>
				</div>
			</div>
		</footer>

	</div>
</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<?= HTML::getLogout();?>

<!-- Bootstrap core JavaScript-->


<script type="text/javascript">
    class hvyChart{
        constructor(){
            this.data = <?php echo  $data;?>
        }
    }
    let hvy = new hvyChart();

    // console.log(hvy.data);

</script>





<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<?= HTML::getScripts('other',['sidebarStatus.js'])?>

<?= HTML::getScripts($page);?>
<!-- Demo scripts for this page-->

<script type="text/javascript">

	let sidebar =$('ul.sidebar');
	let sidebarProcess = new SidebarStatus(sidebar);
    $('#sidebarToggle').click(function () {
			sidebarProcess.toggled();
        })
	;
</script>








</body>

</html>
