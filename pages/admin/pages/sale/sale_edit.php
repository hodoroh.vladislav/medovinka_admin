<?php

use Src\App\App;
use Src\Core\Html\BootstrapForm;



if (isset($_GET['set']))
    if ($_GET['set'] === 'true')
        if (!empty($_POST)){
            if (App::getInstance()->getDb()->update(
                'sales',
                [

                    'product',
                    'date',
                    'quantity',
                    'price',
                    'description',
                ],
                [
                    App::getInstance()->getTable('products')->getMerchandiseId($_POST['selectProduct'])->merchandise_id,
                    '\''.$_POST['date'].'\'',
                    $_POST['quantity'],
                    $_POST['price'],
                    '\''.((empty($_POST['description']))?$_POST['selectProduct']:$_POST['description']).'\''
                ],"id", $_GET["id"]
//
            )){
                echo '<div class="alert alert-success">
				Добавленно
			</div>';
            }else{
                echo '<div class="alert alert-danger">
				Ошибка
			</div>';
            }
        }else{
            echo '<div class="alert alert-warning">
				Нечего добавлять
			</div>';
        }
$post = App::getInstance()->getTable('Sales')->findWithName($_GET['id']);
$form = new BootstrapForm($post);

?>


<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Главная</a>
            </li>
            <li class="breadcrumb-item active">Редактирование продаж</li>
        </ol>
	    <form method="post" action="?page=sale.sale_edit&id=<?=$_GET['id']?>&set=true">

		    <div class="form-group">
			    <label for="form_category" class="font-weight-bold">Выбрать Товар</label>
			    <select name="selectProduct" class="form-control"  data-live-search="true" id="sProdut">
                    <?php foreach (App::getInstance()->getTable('products')->getMerchandise() as $data):?>

					    <option value="<?=$data->name?>" <?php if ($data->name === $post->product)echo ' selected="selected" '?>><?=$data->name?></option>

                    <?php endforeach; ?>
			    </select>
		    </div>		    <?= $form->input('price','Цена'); ?>
		    <?= $form->input('quantity','Количество'); ?>
            <?= $form->input('date','Дата',['type' => 'date']); ?>

		    <?= $form->input('description','Описание',['type' => 'textarea']); ?>
            <?php //$form->input('date','Contenu',['type' => 'textarea']);
            ?>

            <?= $form->getButtons()?>

        </form>


    <!-- Sticky Footer -->
    <footer class="sticky-footer">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>Copyright © Medovinka 2018</span>
            </div>
        </div>
    </footer>

</div>