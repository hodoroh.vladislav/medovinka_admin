<?php


use Src\App\App;
use Src\Core\Html\BootstrapForm;
use Src\Core\Html\HTML;

$form = new BootstrapForm();

if (isset($_GET['upload']))
	if ($_GET['upload'] === 'true')
		if (!empty($_POST)){
            if (isset($_POST['selectProduct'])){
                if (App::getInstance()->getDb()->insert(
                    'sales',
                    [
                        'id',
                        'product',
                        'date',
                        'quantity',
                        'price',
                        'description',
                    ],
                    [
                        'NULL',
                        App::getInstance()->getTable('products')->getMerchandiseId($_POST['selectProduct'])->merchandise_id,
                        '\''.$_POST['date'].'\'',
                        $_POST['quantity'],
                        $_POST['price'],
                        '\''.((empty($_POST['description']))?$_POST['selectProduct']:$_POST['description']).'\''
                    ]
//
                )){
                    echo '<div class="alert alert-success">
				Добавленно.
			</div>';
                }else{
                    echo '<div class="alert alert-danger">
				Ошибка.
			</div>';
                }
            }else{
                echo '<div class="alert alert-warning">
				    <h3>
				        Добавте продукцию перед тем как внести продажу.
                    </h3>
                    <p>
                        <a href="?page=merchandise.merchandise_add">Ссылка</a>
                    </p>
			    </div>';
            }
        }
        else{
            echo '<div class="alert alert-warning">
				 Заполните форму.
			    </div>';
        }


?>
<?= HTML::breadcrumb('Таблица','Добавить продажу');?>
<form action="?page=sale.sale_add&upload=true" method="post" enctype="multipart/form-data"
      id="sale_add" class="mb-4"
              onsubmit="return form.run(); " >

	<div class="form-group">
		<label for="form_category" class="font-weight-bold">Выбарть товар</label>
	<select name="selectProduct" class="form-control"  data-live-search="true" id="sProdut">
		<?php foreach (App::getInstance()->getTable('products')->getMerchandise() as $post):?>

			<option value="<?=$post->name?>"><?=$post->name?></option>

		<?php endforeach; ?>
	</select>
	</div>
    <?=$form->input('quantity','Количество',['type' => 'number','other'=> 'step="0.1"'])?>

	<?=$form->input('price','Цена',['type' => 'number','other'=> 'step="0.01"'])?>

	<?=$form->input('date','Дата',['type' => 'date','other' => 'value="'.date('Y-m-d').'"'])?>

	<?=$form->input('description','Описание',['type' => 'textarea'])?>


    <?= $form->getButtons()?>


		</form>


        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Medovinka 2018</span>
                </div>
            </div>
        </footer>
<?=   HTML::getScripts('other',['merchandiseAdd_update.js']);?>

<script type="text/javascript">
    form = new FormValidation($('#sale_add'));

</script>