<?php
use Src\Core\Html\BootstrapForm;
use Src\App\App;
use Src\Core\Html\HTML;
use Src\Core\Upload\Upload;

echo HTML::getScripts('other',['merchandiseAdd_update.js']);

//Upload::getInstance()->removeAllFiles([],['method'=> 'dir','path'=> 'img']);


if (isset($_GET['upload']))
    if ($_GET['upload'] === 'true')
        if (!empty($_POST)) {
            $res = Upload::getInstance()->update($_GET['id'],'productimg');
            if ($res) {
                echo '<div class="alert alert-success">
				Добавленно
			</div>';
            }else{
                echo '<div class="alert alert-danger">
				Ошибка
			</div>';
            }


        }else
        {
            echo '<div class="alert alert-warning">
				Нечего добавлять, заполните форму.
			</div>';
        }

$post = App::getInstance()->getTable('Products')->findEdit($_GET['id']);

$form = new BootstrapForm($post);
$file = 'http://'.$_SERVER['SERVER_NAME']."/medovinka_fremote/img/productimg/".Upload::getInstance()->getImg($_GET['id']);
?>

<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
<?=HTML::breadcrumb('Таблица','Редакт. продажи')?>

        <form method="post" action="?page=merchandise.merchandise_edit&id=<?=$_GET['id']?>&upload=true" enctype="multipart/form-data" id="merchandise_edit" class="mb-4"
              onsubmit=" return form.run();" >

            <?= $form->input('name','Имя Товара'); ?>

	        <div class="form-group">
		        <label for="form_category" class="font-weight-bold">Выбирите категорию</label>
		        <select name="product_id" id="form_category" class="form-control"  data-live-search="true">
                    <?php foreach (App::getInstance()->getTable('Products')->getCategories() as $category ):?>
				        <option data-tokens="<?=$category->product?>" <?= ($category->product === $post->product)?'selected="selected"':''?>><?= $category->product?></option>
                    <?php endforeach;?>
		        </select>
	        </div>
            <?=$form->input('price','Цена',['type' => 'number','other' => 'step="0.01"'])?>

            <?=$form->input('quantity','Количество',['type' => 'number','other' => 'step="1"'])?>

	        <?= $form->input('description','Описание',['type' => 'textarea']); ?>



	        <div class="form-group">
                <label  class="font-weight-bold">Образ</label>

                <div class="product_img">
			        <img style="width: 480px" src="<?=file_exists("/var/www/html/medovinka_fremote/img/productimg/".Upload::getInstance()->getImg($_GET['id']))?$file:"https://via.placeholder.com/350x151";?>" alt="">
                </div>
		        <br>
		        <label for="form_picture " class="font-weight-bold">Выбрать образ</label>
		        <br>
		        <input type="file" id="form_picture" class="form_category" name="picture">

	        </div>


            <?= $form->getAvailable($post->available === '0','btn-danger')?>






            <?= $form->getButtons()?>

        </form>


        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Medovinka 2018</span>
                </div>
            </div>
        </footer>

    </div>


	<script type="text/javascript">


        form = new FormValidation($('#merchandise_edit'));
	</script>

