<?php

use Src\App\App;
use Src\Core\Html\HTML;
//$post = App::getInstance()->getTable('Products')->all();
//$form = new BootstrapForm($post);
//var_dump(App::getInstance()->getTable('Products')->allTable())
if(isset($_GET['update']))
	if ($_GET['update'] === 'success')
        echo '<div class="alert alert-success">
				Update Ok
			</div>'
?>


<!-- Breadcrumbs-->
<?= HTML::breadcrumb('Tables','Merchandise')?>


<div class="card mb-3">
	<div class="card-header"><i class="fas fa-cart-plus"></i> Add new product</div>
	<div class="card-body">
		<a class="btn btn-primary" href="?page=merchandise.add" style="width: 100%">Add new Product</a>
	</div>
</div>
<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Merchandise table
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


                <?= HTML::getTableDesc('thead',["Id","Name","Description","Category","Price","Quantity","Available","Edit"])?>
                <?= HTML::getTableDesc('tfoot',["Id","Name","Description","Category","Price","Quantity","Available","Edit"])?>
                <tbody>
                <?php foreach (App::getInstance()->getTable('Products')->allTable() as $post):?>
                    <tr>
                        <td><?= $post->merchandise_id?></td>
                        <td><?= $post->name?></td>
                        <td><?= $post->description?></td>
                        <td><?= $post->product?></td>
                        <td><?= $post->price?></td>
                        <td><?= $post->quantity?></td>
                        <td ><button id="available_button_id_<?=$post->merchandise_id?>" style="width: 100%" class="available-btn btn btn-success <?= ($post->available === '0')?'btn-danger': ''; ?>">Available</button></td>
	                    <td><a style='width:100%' class='btn btn-primary' href='admin.php?page=merchandise.edit&id=<?=$post->merchandise_id?>'>Edit</a></td>
                    </tr>


                <?php endforeach; ?>

                </tbody>




            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>

<p class="small text-center text-muted my-5">
    <em>More table examples coming soon...</em>
</p>

<?= HTML::getScripts('other',['changeAvailable.js'])?>

