<?php

use Src\App\App;
use Src\Core\Html\HTML;
//$post = App::getInstance()->getTable('Products')->all();
//$form = new BootstrapForm($post);
//var_dump(App::getInstance()->getTable('Products')->allTable())
if(isset($_GET['update']))
	if ($_GET['update'] === 'success')
        echo '<div class="alert alert-success">
				Добавленно
			</div>'
?>


<!-- Breadcrumbs-->
<?= HTML::breadcrumb('Tables','Merchandise')?>


<div class="card mb-3">
	<div class="card-header"><i class="fas fa-cart-plus"></i> Добавить продукцию</div>
	<div class="card-body">
		<a class="btn btn-primary" href="?page=merchandise.merchandise_add" style="width: 100%">Добавить продукцию</a>
	</div>
</div>
<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Продукция
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">

                <?php $hr = ["Имя","Описание","Категория","Цена","Количество","Доступно","Редакт."];?>
                <?= HTML::getTableDesc('thead',$hr)?>
                <?= HTML::getTableDesc('tfoot',$hr)?>
                <tbody>
                <?php foreach (App::getInstance()->getTable('Products')->allTable() as $post):?>
                    <tr>
                        <td><?= $post->name?></td>
                        <td><?= $post->description?></td>
                        <td><?= $post->product?></td>
                        <td><?= $post->price?></td>
                        <td><?= $post->quantity?></td>
                        <td ><button id="available_button_id_<?=$post->merchandise_id?>" style="width: 100%" class="available-btn btn btn-success <?= ($post->available === '0')?'btn-danger': ''; ?>">Доступно</button></td>
	                    <td><a style='width:100%' class='btn btn-primary' href='?page=merchandise.merchandise_edit&id=<?=$post->merchandise_id?>'>Редактировать</a></td>
                    </tr>


                <?php endforeach; ?>

                </tbody>




            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>

<p class="small text-center text-muted my-5">
    <em>...</em>
</p>

<?= HTML::getScripts('other',['changeAvailable.js'])?>


<script type="text/javascript">
    let product_name = "<?=(isset($_GET['id']))?App::getInstance()->getTable('Products')->getCategoryOfId($_GET['id'])->product:""; ?>";


    document.addEventListener("DOMContentLoaded", function(event) {
        $("#dataTable").dataTable( {
            "search": {
                "search": product_name
            }
        });
    });

</script>