<?php

use Src\Core\Html\BootstrapForm;
use Src\App\App;
use Src\Core\Html\HTML;
echo HTML::getScripts('other',['merchandiseAdd_update.js']);

$form = new BootstrapForm();
if (isset($_GET['upload']))
    if ($_GET['upload'] === 'true')
        if (!empty($_POST))
            if (App::getInstance()->getDb()->insert('product_category',
                [
                    'product_id'    => 'NULL',
                    'product'    => '\''.$_POST['category'].'\''
                ])
            )
                echo '<div class="alert alert-success">
				Категория была добавлена
			    </div>';
            else
                echo '<div class="alert alert-danger">
				Категория не была добавлена
			    </div>';



?>
<?=HTML::breadcrumb('Категория','Категория')?>

<form action="?page=merchandise.merchandise_category_add&upload=true" method="post" enctype="multipart/form-data"
      id="category_add" class="mb-4"
      onsubmit=" return form.run();" >
    <?= $form->input('category','Имя категории') ?>



    <?= $form->getAvailable()?>



    <?= $form->getButtons()?>

</form>


<script type="text/javascript">


    form = new FormValidation($('#category_add'));
</script>