<?php

use Src\App\App;
use Src\Core\Html\HTML;



?>
<?= HTML::breadcrumb('Tables','Categories')?>

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-book"></i>
        Доюавить категорию
    </div>
    <div class="card-body">
        <a href="?page=merchandise.merchandise_category_add" class="btn btn-primary" style="width: 100%">Добавить категорю</a>
    </div>
</div>


<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Продажи
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


               <?= HTML::getDescTable(['Категория','Количество','Ссылка'])?>

                <tbody>
                    <?php foreach (App::getInstance()->getTable('Categories')->getCategories() as $post):?>
                        <tr>
                            <td><?= $post->product?></td>
                            <td><?= $post->count?></td>
                            <td style="width: 30%">
                                <a href="?page=merchandise.merchandise_table&id=<?=$post->product_id?>">
                                    <button class="btn <?=($post->product_id === null)?' btn-secondary':'btn-primary'?>" <?=($post->product_id == null)?'disabled':''?> style="width: 100%; ;">
                                        <span>
                                            Смотреть : <?= $post->product?>
                                        </span>
                                    </button>
                                </a>
                            </td>

                        </tr>
                    <?php endforeach;?>
                </tbody>




            </table>
        </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>


