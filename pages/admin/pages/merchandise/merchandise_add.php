<?php
use Src\Core\Html\BootstrapForm;
use Src\App\App;
use Src\Core\Html\HTML;
use Src\Core\Upload\Upload;

//
//$post = App::getInstance()->getTable('Products')->findEdit($_GET['id']);
$form = new BootstrapForm();

 echo HTML::getScripts('other',['merchandiseAdd_update.js']);
//App::getInstance()->getTable('products')->deleteFromBd(13,ROOT.'/public/img');

if (isset($_GET['upload']))
	if ($_GET['upload'] === 'true')
        if (!empty($_POST)){
            if (isset($_POST['select_category'])){
                if (Upload::getInstance()->uploadFile('productimg'))
                    App::getInstance()->getDb()->insert(
                        'merchandise',
                        [
                            'merchandise_id'    => 'NULL',
                            'product_id'        => current(App::getInstance()->getTable('categories')->getCategoryId($_POST['select_category']))->product_id,
                            'name'              => '\''.$_POST['product'].'\'',
                            'description'       => '\''.$_POST['description'].'\'',
                            'price'             =>  $_POST['price'],
                            'quantity'          => $_POST['quantity'],
                            'img'               => '\''.current(Upload::getInstance()->file_name).'\'',
                            'available'         => $_POST['available']
                        ]);
            }
            else{
                echo '<div class="alert alert-danger">
				    Добавте категорию перед добавлением продуции
				    <h3>
				       <a href="?page=merchandise.merchandise_category_add"> Ссылка</a>
                    </h3>
			    </div>';
            }

}
        else
        {
            echo '<div class="alert alert-warning">
				Нечего добавлять, заполните форму.
			</div>';
        }

?>
<script type="text/javascript"></script>

        <!-- Breadcrumbs-->
	<?= HTML::breadcrumb('Таблица','Товар')?>

        <form action="?page=merchandise.merchandise_add&upload=true" method="post" enctype="multipart/form-data"
              id="merchandise_add" class="mb-4"
              onsubmit=" return form.run();" >
        <?= $form->input('product','Товар') ?>

		<div class="form-group">
			<label for="form_category" class="font-weight-bold">Выбрать категорию</label>
			<select name="select_category" class="form-control"  data-live-search="true">
                <?php foreach (App::getInstance()->getTable('Products')->getCategories() as $category ):?>
					<option data-tokens="<?=$category->product?>"><?= $category->product?></option>
                <?php endforeach;?>
			</select>
		</div >

		<?=$form->input('price','Цена',['type' => 'number','other'=> 'step="0.01"'])?>

		<?=$form->input('quantity','Количество',['type' => 'number','other'=> 'step="0.1"'])?>

        <?=$form->input('description','Описание',['type' => 'textarea'])?>

		<div class="form-group">
			<label for="form_picture " class="font-weight-bold">Выбрать образ</label>
			<br>
			<input type="file" id="form_picture" class="form_category" name="picture">

		</div>

            <?= $form->getAvailable()?>




            <?= $form->getButtons()?>
		</form>


        <!-- Sticky Footer -->
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © Medovinka 2018</span>
                </div>
            </div>
        </footer>

<script type="text/javascript">


    form = new FormValidation($('#merchandise_add'));
</script>

