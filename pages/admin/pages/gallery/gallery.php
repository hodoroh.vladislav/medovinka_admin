<?php

use Src\App\App;
use Src\Core\Html\HTML;

//$post = App::getInstance()->getTable('gallery')->getAllImages();
//$form = new BootstrapForm($post);

if (isset($_GET['delete']))
    if (isset($_GET['id'])){
        try{
            $file = App::getInstance()->getTable('gallery')->find($_GET['id']);
            if ($file != false){
                if (App::getInstance()->getTable('gallery')->delete($_GET['id'])){
                    echo '<div class="alert alert-success mt-lg-3 mb-lg-3">BD proc Ok</div>';
                    if (unlink('/var/www/html/medovinka_fremote/img/gallery/'.$file->img)){
                        echo '<div class="alert alert-success mt-lg-3 mb-lg-3"> Delete file: Ok</div>';
                    }
                }
                else{
                    echo '<div class="alert alert-warning">Some errors occured during deleting file from db</div>';
                }
            }

        }catch(ErrorException $error){
            var_dump($error);
        }
    }else{
        echo '<div class="alert alert-warning mt-lg-3 mb-lg-3"> No Id!</div>';
    }

//var_dump($post);
?>
<?= HTML::breadcrumb('Gallery','Gallery')?>
<style>
    .gallery-block.grid-gallery{
        padding-bottom: 60px;
        padding-top: 60px;
    }

    .gallery-block.grid-gallery .heading{
        margin-bottom: 50px;
        text-align: center;
    }

    .gallery-block.grid-gallery .heading h2{
        font-weight: bold;
        font-size: 1.4rem;
        text-transform: uppercase;
    }

    .gallery-block.grid-gallery a:hover{
        opacity: 0.8;
    }

    .gallery-block.grid-gallery .item img{
        box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.15);
        transition: 0.4s;
    }

    .gallery-block.grid-gallery .item{
        margin-bottom: 20px;
    }

    @media (min-width: 576px) {

        .gallery-block.grid-gallery .scale-on-hover:hover{
            transform: scale(1.05);
            box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.15) !important;
        }
    }
</style>

<div class="container-fluid">
    <div class="row mb-3">
        <div class="col col-lg-12">
            <a href="?page=gallery.gallery_add" class="btn btn-primary w-100">Add image</a>
        </div>
    </div>

    <section class="gallery-block grid-gallery">
        <div class="container-fluid">
            <div class="card-deck">

                <div class="row">


                    <?php foreach (App::getInstance()->getTable('gallery')->parse() as $post):?>

                        <div class="col-md-6 col-lg-4 mb-3">
                            <div class="card">
                                <div class="card-header">
                                    <ul class="nav nav-tabs card-header-tabs">
                                        <li class="nav-item">

                                        </li>
                                    </ul>
                                </div>
                                <div >
                                    <?php $image = App::getInstance()->getTable('gallery')->showImage($post->img);
                                    ?>
                                    <a class="lightbox" href="<?= $image?>" class="lightbox">
                                        <img async=on decoding="async" class="img-fluid card-img-top image scale-on-hover"   src="<?=$image?>" alt="">
                                    </a>
                                </div>
                                <div class="card-body">
                                    <div >
                                        <input class="form-control" type="number" value="<?= $post->img_order?>" id="example-number-input">
                                    </div>

                                    <button id="available_button_id_<?=$post->id?>"  class="available-btn btn  w-100 btn-lg btn-success  <?= ($post->available === '0')?'btn-danger': '';  ?>">Available</button>
                                    <div class="row mt-1">
                                        <div class="col-6 col-md-6 col-lg-6 ">
                                            <a href="" class="btn btn-primary w-100">Edit</a>
                                        </div>
                                        <div class="col-6 col-md-6 col-lg-6  border-warning">
                                            <a href="?page=gallery.gallery&delete=true&id=<?=$post->id?>"class="btn btn-secondary w-100">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <!--                                    <small class="text-muted">Last updated 3 mins ago</small>-->
                                </div>

                            </div>

                        </div>
                    <?php endforeach;?>


                </div>

            </div>
        </div>
    </section>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.10.0/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>


</div>
<?= HTML::getScripts('other',['changeGalleryAvailable.js'])?>

