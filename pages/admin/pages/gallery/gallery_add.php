<?php

use Src\App\App;
use Src\Core\Html\BootstrapForm;
use Src\Core\Html\HTML;
use Src\Core\Upload\Upload;

 echo HTML::getScripts('other',['formValidation.js']);

if (isset($_GET['upload'])){
    if ($_GET['upload'] === 'true')
    {
        if (!empty($_POST)){
            if (Upload::getInstance()->uploadFile('gallery')){

                App::getInstance()->getDb()->insert(
                    'gallery',
                    [
                        'id' => 'NULL',
                        'img' => '\''.current(Upload::getInstance()->file_name).'\'',
                        'available' => $_POST['available'],
                        'width' => Upload::getInstance()->getImgInfo(Upload::getInstance()->file_name,"width"),
                        'height' => Upload::getInstance()->getImgInfo(Upload::getInstance()->file_name,"height")
                    ]
                );
            }
        }
    }
}
//                        'width' => Upload::getInstance()->getWidth()


?>

<?= HTML::breadcrumb('Gallery','Add Image')?>
<form action="?page=gallery.gallery_add&upload=true" method="post" enctype="multipart/form-data"
      id="gallery_add" class="mb-4"
      onsubmit=" return form.run();" >


    <div class="form-group">
        <label for="form_addImg " class="font-weight-bold">Select Img</label>
        <br>
        <input type="file" id="form_addImg" class="form_category btn btn-secondary" name="picture">

    </div>

    <div class="form-group">
        <label for="form_available" class="font-weight-bold">Available</label>
        <br>
        <button type="button" value="false" class="btn btn-primary btn-success " id="available_btn">Available</button>

    </div>



    <div class="row">
        <div class="col col-lg-12 text-right">
            <button class="btn btn-warning text-white" type="reset">Resset</button>
            <button class="btn btn-primary" type="submit" value="Sauvegarder" >Upload</button>
            <a href="?p=home" class="btn btn-secondary">Back</a>

        </div>
    </div>
</form>

<script type="text/javascript">


    form = new FormValidation($('#gallery_add'));
</script>






