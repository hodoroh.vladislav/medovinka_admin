<?php
use Src\App\App;
use Src\Core\Html\HTML;

?>


          <!-- Breadcrumbs-->
<?= HTML::breadcrumb('Sales','Sales')?>


<!-- DataTables Example -->
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
                Sales Table
            </div>
            <div class="card-body">
              <div class="table-responsive">
                  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">


                      <?= HTML::getTableDesc('thead')?>
                      <?= HTML::getTableDesc('tfoot')?>
                      <tbody>
                      <?php foreach (App::getInstance()->getTable('Sales')->allTable() as $post):?>
                      <tr>
                          <?= HTML::getRow($post,false)?>

                      </tr>
                      <?php endforeach; ?>

                      </tbody>




                  </table>
              </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
          </div>


