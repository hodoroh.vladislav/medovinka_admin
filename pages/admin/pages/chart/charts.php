<?php


use Src\App\App;
use Src\Core\Html\HTML;
$data = json_encode(App::getInstance()->getTable('statistic')->getStock());

?>
        <!-- Breadcrumbs-->
     <?= HTML::breadcrumb('Dashboard','Charts')?>


        <!-- Area Chart Example-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-chart-area"></i>
                Sales Chart</div>
            <div class="card-body">
                <canvas id="myAreaChart" width="100%" height="30"></canvas>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-chart-bar"></i>
                        Sales Chart</div>
                    <div class="card-body">
                        <canvas id="myBarChart" width="100%" height="50"></canvas>
                    </div>
                    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-chart-pie"></i>
                        No idea Chart</div>
                    <div class="card-body">
                        <canvas id="myPieChart" width="100%" height="100"></canvas>
                    </div>
                    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                </div>
            </div>
        </div>
        <div class="row">
	        <div class="col col-lg-4">
		        <div class="card mb-3">
			        <div class="card-header">
				        <i class="fas fa-chart-pie"></i>
				        Stock Chart</div>
			        <div class="card-body">
				        <canvas id="stockChart" width="100%" height="100"></canvas>
			        </div>
			        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
		        </div>
	        </div>
	        <div class="col col-lg-4"></div>
	        <div class="col col-lg-4"></div>
        </div>



<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>-->
        <script type="text/javascript">

	        class stockChart{
	            constructor(){
	                this.data = {
	                    'label': ['label1','label2','label3','label4','label5'],
		                'data': [5,15,4,25,5]
	                }
	            }
	        }

	        let stock = new stockChart();

        </script>


<!-- /.content-wrapper -->

