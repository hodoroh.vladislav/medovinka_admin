<?php



?>




<div class="card mb-3">
    <div class="card-header"><i class="fas fa-book"></i> Add Category</div>
    <div class="card-body">
        <a class="btn btn-primary" href="?page=tasks.settings.task_category_add" style="width: 100%">Add new Category</a>
    </div>
</div>

<div class="card mb-3">
    <div class="card-header"><i class="fas fa-list"></i> Add Task</div>
    <div class="card-body">
        <a class="btn btn-primary" href="?page=tasks.settings.task_add" style="width: 100%">Add new Task</a>
    </div>
</div>