<?php


use Src\Core\Html\HTML;

?>
<link rel="stylesheet" href="css/card_task.css">

<?= HTML::breadcrumb('Tasks','Categories')?>
<div class="row card_style">
    <div class="col col-lg-8 card_style_categories_container">


        <div class="card mb-3 " >
            <div class="card-body ">
                <h5 class="card-title">General category</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text d-flex justify-content-between align-items-center">Some quick example text to build on the card title and make up the bulk of the card's content.
                </p>
                <div class="row mb-3">
                    <div class="col col-lg-4">
                        <span class="badge badge-primary badge-pill">0</span>

                    </div>
                    <div class="col col-lg-4">
                        <span class="badge badge-warning badge-pill">0</span>

                    </div>
                    <div class="col col-lg-4">
                        <span class="badge badge-danger badge-pill">0</span>

                    </div>
                </div>
                <a href="#" class=" btn card-link  btn-primary " >Card link</a>
            </div>
        </div>





            <div class="card mb-3 " >
                <div class="card-body ">
                    <h5 class="card-title">General category</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                    <p class="card-text d-flex justify-content-between align-items-center">Some quick example text to build on the card title and make up the bulk of the card's content.
                    </p>
                    <div class="row mb-3">
                        <div class="col col-lg-4">
                            <span class="badge badge-primary badge-pill">0</span>

                        </div>
                        <div class="col col-lg-4">
                            <span class="badge badge-warning badge-pill">0</span>

                        </div>
                        <div class="col col-lg-4">
                            <span class="badge badge-danger badge-pill">0</span>

                        </div>
                    </div>
                    <a href="#" class=" btn card-link  btn-primary " >Card link</a>
                </div>
            </div>




            <div class="card mb-3 " >
            <div class="card-body ">
                <h5 class="card-title">General category</h5>
                <h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
                <p class="card-text d-flex justify-content-between align-items-center">Some quick example text to build on the card title and make up the bulk of the card's content.
                </p>
                <div class="row mb-3">
                    <div class="col col-lg-4">
                        <span class="badge badge-primary badge-pill">0</span>

                    </div>
                    <div class="col col-lg-4">
                        <span class="badge badge-warning badge-pill">0</span>

                    </div>
                    <div class="col col-lg-4">
                        <span class="badge badge-danger badge-pill">0</span>

                    </div>
                </div>
                <a href="#" class=" btn card-link  btn-primary " >Card link</a>
            </div>
        </div>



    </div>


    <div class="col col-lg-4">
        <div class="row">
            <div class="col col-lg-12">
                <?= HTML::breadcrumb('Tasks','Last Added')?>
            </div>
        </div>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">List group item heading</h5>
                    <small>3 days ago</small>
                </div>
                <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                <small>Donec id elit non mi porta.</small>
            </a>
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">List group item heading</h5>
                    <small class="text-muted">3 days ago</small>
                </div>
                <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                <small class="text-muted">Donec id elit non mi porta.</small>
            </a>
            <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">List group item heading</h5>
                    <small class="text-muted">3 days ago</small>
                </div>
                <p class="mb-1">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</p>
                <small class="text-muted">Donec id elit non mi porta.</small>
            </a>
        </div>


    </div>
</div>
