<?php

use Src\App\App;
use Src\Core\Html\HTML;
use Src\Core\Statistics\Stats;


?>



    <!-- Sidebar -->



            <!-- Breadcrumbs-->
<?=HTML::breadcrumb('Dashboard','Overview')?>

            <!-- Icon Cards-->
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa fa-user-circle"></i>
                            </div>
                            <div class="mr-5"> Views for today :<span class="font-weight-bold"> <?=  (!empty(Stats::getInstance()->getViews()))?Stats::getInstance()->getViews()[0]->views:0;?> </span> </div>
                            <div class="mr-5"> Visitors for today <span class="font-weight-bold"> : <?= (!empty(Stats::getInstance()->getViews()))?Stats::getInstance()->getViews()[0]->hosts:0;?></span></div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <span class="float-left font-weight-bold">View Details</span>
                            <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-warning o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-list"></i>
                            </div>
                            <div class="mr-5 ">11 New Tasks!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <span class="float-left font-weight-bold">View Details</span>
                            <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-shopping-cart"></i>
                            </div>
                            <div class="mr-5">
	                            <span class="font-weight-bold"><?=  App::getInstance()->getTable('Statistic')->getSalesQuantity(null)['qtty'] ?></span> Sales for current year
                            </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <span class="float-left font-weight-bold">View Details</span>
                            <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-danger o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fas fa-fw fa-life-ring"></i>
                            </div>
                            <div class="mr-5">13 New Tickets!</div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <span class="float-left font-weight-bold">View Details</span>
                            <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                        </a>
                    </div>
                </div>
            </div>

            <!-- Area Chart Example-->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-chart-area"></i>
                    Area Chart Example</div>
                <div class="card-body" >
                    <canvas id="myAreaChart" width="100%" height="30%" ></canvas>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>

<div class="card mb-3">
	<div class="card-header">
		<i class="fas fa-table"></i>
		Add Product / Sale
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col col-lg-6">
				<a href="?page=sale.add" style="width: 100%" class="btn btn-primary">Add new Sale</a>
			</div>
			<div class="col col-lg-6">
				<a href="?page=merchandise.add" style="width: 100%" class="btn btn-primary">Add new Product</a>
			</div>
		</div>

	</div>
</div>

            <!-- DataTables Example -->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Sales history</div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">



						<?= HTML::getTableDesc('thead')?>
						<?= HTML::getTableDesc('tfoot')?>
	                        <tbody>
						<?php foreach (App::getInstance()->getTable('Sales')->allTable() as $post):?>


							<tr>
                                <?= HTML::getRow($post,false)?>

							</tr>

						<?php endforeach; ?>

	                        </tbody>



                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>

    <!-- /.content-wrapper -->

<!-- /#wrapper -->
