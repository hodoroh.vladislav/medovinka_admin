<?php
use Src\App\App;
use Src\Core\Auth\DBAuth;


if(!empty($_POST)){
    $auth = new DBAuth(App::getInstance()->getDb());
    if ($auth->login($_POST['username'],$_POST['password']))
        echo '
			<div class="alert alert-success">
				Identifiants Corrects
			</div>
			';
}



?>

<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form method="post">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="text" name="username" id="inputLogin" class="form-control" placeholder="Login" required="required" autofocus="autofocus">
                        <label for="inputLogin">Login</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="required">
                        <label for="inputPassword">Password</label>
                    </div>
                </div>
<!--                <div class="form-group">-->
<!--                    <div class="checkbox">-->
<!--                        <label>-->
<!--                            <input type="checkbox" value="remember-me">-->
<!--                            Remember Password-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
                <button type="submit" class="btn btn-primary btn-block" >Login</button>
            </form>

        </div>
    </div>
</div>

