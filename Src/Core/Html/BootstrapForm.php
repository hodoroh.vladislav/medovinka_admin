<?php

namespace Src\Core\Html;

class BootstrapForm extends Form
{

    protected function surround($html)
    {
        return '<div class="form-group">'. $html .'</div>';
    }
    public function getButtons(){
        return '
        <div class="row">
                <div class="col col-lg-12 text-right">
	                <button class="btn btn-warning text-white" type="reset">Resset</button>
	                <button class="btn btn-primary" type="submit" value="Sauvegarder" >Update</button>
                    <a href="?p=home" class="btn btn-secondary">Back</a>

                </div>
            </div>
        ';
    }
    public function getAvailable($bool = false,$class = ''){
        return '
        <div class="form-group">
        <label for="form_available" class="font-weight-bold">Доступно</label>
        <br>
        <button type="button" value="false" class="btn btn-primary btn-success '.($bool?$class:'').'" id="available_btn">Доступно</button>

        </div>
        ';

    }
    public function input($name,$label,$options = [])
    {
        $other = isset($options['other'])?$options['other']:'';
        $type = isset($options['type'])? $options['type']: 'text';
        $nLabel ='<label class="font-weight-bold">'.ucfirst($label). '</label>';
//        var_dump($type);
        if ($type === 'textarea')
            $input = '<textarea class="form-control" style="height:250px;" name="'.$name.'">'.$this->getValue($name).'</textarea>';
        else
            $input = '<input type="'.$type.'" '.$other.' name="'.$name.'" class="form-control" value="'.$this->getValue($name).'" placeholder="Enter  '.$name.'">';

        $html = $nLabel.$input;
        return $this->surround(
            $html
        );
    }

    public function submit()
    {
        return $this->surround('<button type="submit" class="btn btn-secondary">Envoyer</button>');
    }

}