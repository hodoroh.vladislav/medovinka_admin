<?php


namespace Src\Core\Html;
use Src\App\App;

class HTML
{
    private static $activeElement;
    private static $tableColumns;

    public static function getNav()
    {
        return '
            <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        Start Bootstrap
                    </a>
                </li>
                <li>
                    <a href="#">Dashboard</a>
                </li>
                <li>
                    <a href="#">Shortcuts</a>
                </li>
                <li>
                    <a href="#">Overview</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Services</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
                <li>
                    <a href="admin.php?page=login">Admin</a>
                </li>
            </ul>
        </div>';
    }

    protected static function getActive($Element)
    {
        if ($Element === self::$activeElement)
            return 'active';

    }


    public static function breadcrumb($section = 'Dashboard',$name){
        $link = "#";
        switch ($section){
            case "Dashboard":
                $link= "?page=tables.table_list&link=all";
                break;
            case "Tables":
                $link = "?page=tables.table_list&link=tables";
                break;
            case 'Sales':
                $link = "?page=tables.table_list&link=sales";
                break;
            case 'Tasks':
                $link = "?page=tables.table_list&link=tasks";
                break;
        }

        return '
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="'.$link.'">'.$section.'</a>
            </li>
            <li class="breadcrumb-item active">'.$name.'</li>
          </ol>

        ';
    }

    public static function getSideBar($activeElement)
    {
        self::$activeElement = $activeElement;

        return'    
        <ul class="sidebar navbar-nav '.(($_SESSION["sidebar"])?'toggled':'').'">
        <li class="nav-item '.self::getActive('home').'">
            <a class="nav-link" href="admin.php?page=index">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item dropdown '.self::getActive('tables').'">
            <a class="nav-link dropdown-toggle" href="#" id="tablesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-table"></i>
                <span>Tables</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="tablesDropdown">
                <h6 class="dropdown-header">Merchandise tables</h6>
                <a class="dropdown-item" href="?page=merchandise.merchandise_table">Products</a>
                <a class="dropdown-item" href="?page=merchandise.merchandise_categories">Categories</a>
                
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Other:</h6>
                <a class="dropdown-item" href="?page=tables.tables">Sales</a>
            </div>
        </li>
        <li class="nav-item dropdown '.self::getActive('tasks').'">
            <a class="nav-link dropdown-toggle" href="#" id="tablesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-list"></i>
                <span>Tasks</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="tablesDropdown">
                <h6 class="dropdown-header">Tasks</h6>
                <a class="dropdown-item" href="?page=tasks.task_table">Categories</a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Settings</h6>
                <a class="dropdown-item" href="?page=tasks.task_settings">Settings</a>
                

            </div>
        </li>
        <li class="nav-item '.self::getActive('charts').'">
            <a class="nav-link" href="admin.php?page=chart.charts">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>Charts</span></a>
        </li>
        <li class="nav-item '.self::getActive('gallery').'">
            <a class="nav-link" href="admin.php?page=gallery.gallery">
                <i class="fas fa-fw fa-images"></i>
                <span>Gallery</span></a>
        </li>
        <li class="nav-item dropdown'.self::getActive('misc').'">
              <a class="nav-link dropdown-toggle" href="#" id="miscDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-fw fa-cogs"></i>
                <span>WebSite Settings</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="miscDropdown">
                <h6 class="dropdown-header">Misc</h6>
                <a class="dropdown-item" href="#">Gallery settings</a>
                
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header">Other:</h6>
                
            </div>
        </li>
    </ul>';
    }

    public static function getScripts($page,$options = []){
        $page_arr = [];
        preg_match_all('/[a-zA-Z_].[a-zA-Z-_]*/', $page, $page_arr);
        $proc = current($page_arr);


        if ($proc !== false)
        switch ($proc[0]) {
            case 'index':
                return self::fgScript([
                    'chart' => ['chart-area-demo.js'],
                    'tables'=>['datatables-demo.js']
//                    'jq' => 'https://code.jquery.com/jquery-3.3.1.min.js',
//                    'poopper' => 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
//                    'bootstrap' => 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'
                ]);
                break;
            case 'chart':
                return self::fgScript([
                    'charts' => ['chart-area-demo.js','chart-bar-demo.js','chart-pie-demo.js','stockChart.js'],
                ]);
                break;
            case 'tables':
                return self::fgScript([
                    'tables'=>['datatables-demo.js'],
                ]);
                break;
            case 'merchandise':
                return self::fgScript([
                    'tables'=>['datatables-demo.js'],
                ]);
                break;

            case 'other':
                return self::fgScript(['other' => $options]);
                break;
            default:
//                return self::fgScript([
//                    'tables'=>['datatables-demo.js'],
//                ]);

        }
    }

    private static function fgScript($config){
        $scripts = '';


        foreach ($config as $key => $value){
            if ($key === 'other'){
                if (is_array($value))
                    foreach ($value as $secondValue) {
                        if (file_exists('js/'.$secondValue))
                            $scripts .= '<script src="js/'.$secondValue.'"></script>';
                    }
                else
                    $scripts .='<script src="js/'.$value.'"></script>';
            }
            else
            {
                if (is_array($value))
                    foreach ($value as $secondValue) {
                        if (file_exists('js/demo/'.$secondValue))
                            $scripts .= '<script src="js/demo/'.$secondValue.'"></script>';
                    }
                else
                    $scripts .='<script src="js/demo/'.$value.'"></script>';
            }
        }
        return $scripts;
    }

    private static function getNavbarSearch($disabled)
    {
        if ($disabled) {
            return ' 
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
        <div class="input-group">
            <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" type="button">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form> ';
        }else{
            return'
            <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            
            </form>';
        }
    }

    public static function getAdminNav()
    {
        return '
        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="admin.php?page=index">Medovinka Admin Panel</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
   
   '.self::getNavbarSearch(false).'

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <span class="badge badge-danger">9+</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
            <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-envelope fa-fw"></i>
                <span class="badge badge-danger">7</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">Settings</a>
                <a class="dropdown-item" href="#">Activity Log</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>
        </li>
    </ul>

</nav>

        ';
    }

    public static function getLogout()
    {
        return'<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="admin.php?page=logout">Logout</a>
            </div>
        </div>
    </div>
</div>';
    }

    public static function surround($tag, $html,$method = 1)
    {
        if ($method === 1) {
            return str_replace('{HTML}', $html, $tag);
        }
        else if ($method === 2) {
            return '<' . $tag . '>' . $html . '</' . $tag . '>';
        }
    }

    public static function getRow($objectRow,$method = true,$misc = null)
    {
        if (!$method)
            return  "  
  
      
            <td>{$objectRow->id}</td>
            <td>{$objectRow->product}</td>
            <td>{$objectRow->quantity}</td>
            <td>{$objectRow->price}</td>
            <td>{$objectRow->date}</td>
            <td>{$objectRow->description}</td>
            <td><a style='width:100%' class='btn btn-primary' href='admin.php?page=sale.sale_edit&id={$objectRow->id}'>Edit</a></td>
               ";
        else{
            $row = '';
            foreach ($objectRow as $key => $value) {
                $row .= '<td>'.$value.'</td>';
            }
            if ($misc === null)
                return self::surround('tr',$row,2);
            else{
                foreach ($misc as $value){
                    $row .= '<td>'.$value.'</td>';
                }
                return self::surround('tr',$row,2);

            }
        }



    }

    /**
     * @param $tag
     * @param null $options
     * @return string
     */
    public static function getTableDesc($tag, $options = null){

        if ($options === null) {
            $tableObject = new \stdClass();
            if (self::$tableColumns === null){
                foreach (App::getInstance()->getTable('Sales')->showColumns() as $th){
                    $tableObject->{$th->Field} = $th->Field;
                }
                self::$tableColumns = $tableObject;
            }



            return  '<'.$tag.'>'.'
                    
            <tr>
                <th>' .ucfirst(self::$tableColumns->id) . '</th>
                <th>' .ucfirst(self::$tableColumns->product). '</th>
                <th>' .ucfirst(self::$tableColumns->quantity). '</th>
                <th>' .ucfirst(self::$tableColumns->price). '</th>
                <th>' .ucfirst(self::$tableColumns->date). '</th>
                <th>' .ucfirst(self::$tableColumns->description).'</th>
                <th>Edit</th>
        </tr>
        
        '.'</'.$tag.'>';
        } else {
            $html = "";
            foreach ($options as $value){
                $html .= "<th>{$value}</th>";
            }
            return "<{$tag}><tr>".$html."</tr></{$tag}>";
        }

    }
    public static function getDescTable($name = array()){
        $html = '';
        foreach ($name as $value){
            $html .= "<th>{$value}</th>";
        }
        return '
            <thead>'.$html.'</thead>
            <tfoot>'.$html.'</tfoot>
            ';
    }




}