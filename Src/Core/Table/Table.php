<?php


namespace Src\Core\Table;

use Src\App\App;
use Src\Core\Database\Database;
use Src\Core\Upload\Upload;

class Table
{
    protected $table;
    protected $db;


    public function __construct(Database $db)
    {
        $this->db = $db;
        if (is_null($this->table)) {
            $parts = explode('\\',get_class($this));
            $class_name = end($parts);
            $this->table = strtolower(str_replace('Table','',$class_name)).'s';
        }

    }
    public function delete($id){
        return $this->execute("delete from {$this->table} where id = {$id}");
    }

    public function find($id)
    {
        return $this->query("SELECT * FROM {$this->table} WHERE id = ?",[$id],true);
    }
    public function all()
    {
        return $this->query('SELECT * FROM '.$this->table);
    }
    public function execute($query){
        return $this->db->execute($query);
    }
    public function query($statement, $attributes = null, $one = false)
    {
        if ($attributes) {
            return $this->db->prepare(
                $statement,
                $attributes,
                str_replace('Table','Entity',get_class($this)),
                $one);
        }

        return $this->db->query(
            $statement,
            str_replace('Table','Entity',get_class($this)),
            $one);

    }

    public function showColumns()
    {
        $tmp = '

        SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE table_name = '.$this->table.'
        AND table_schema = medovinka';
        return App::getInstance()->getDb()->query("DESCRIBE {$this->table}"
        );
    }



}