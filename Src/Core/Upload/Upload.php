<?php


namespace Src\Core\Upload;


use Src\App\App;

class Upload
{
    public static $_instance;

    private $post;
    public $file_name = [];


    public function __construct()
    {

        $_SESSION["msg"] = '';
        $_SESSION['ok_msg'] = '';
    }

    public function getImg($îd){
        return current(App::getInstance()->getDb()->query("select merchandise.img from merchandise where merchandise.merchandise_id = ".$îd))->img;
    }
    public static function getInstance()
    {
        if (self::$_instance === null)
            self::$_instance = new Upload();
        return self::$_instance;
    }
    public static function message($message,$class){
        echo  '
            <div class="alert '.$class.'">
                '.$message.'
            </div>';
    }
    public function uploadFile($directory){


        if (!$this->error()) {
            $flag = false;
            $files = $this->moveFile($directory);
           foreach ($files as $key =>$value){
               if (!$value['success']){
                   $this->removeAllFiles($files);
                    $flag = true;
                    $this->file_name = [];
                    break;
               }
            if ($flag){
                self::message("Ошибка",'alert-danger');
            }
           else{
               self::message("Добавленно",'alert-success');
               return true;
           }

           }
        }
        else
        {
            echo '
            <div class="alert alert-danger">
                ' . $_SESSION['msg'] . '
            </div>';
            return false;
        }
    }

    public function update($id,$directory){
        $flag = false;

        if ($this->error() && (current($_FILES)['error'] !== 4)){
           $_SESSION['msg'] .= "get an error\n";
           self::message($_SESSION['msg'],'danger');
       }

       else if(current($_FILES)['error'] === 4)
        {
            return App::getInstance()->getTable('products')->update(
                'merchandise',$_POST);

        }
        else
        {

            /** @var TYPE_NAME $file
             *Current stored file
             */
            $file = $this->getImg($id);

            /** @var TYPE_NAME $files
             *New file
             */
            $files = $this->moveFile($directory);

            foreach ($files as $key => $value){
                if (!$value['success']){
                    $this->removeAllFiles($files);
                    $flag = true;
                    $this->file_name = [];
                    break;
                }

                    if ($flag){
                        self::message("Error on copy",'alert-danger');
                    }
                    else{
                        if (App::getInstance()->getTable('products')->update('merchandise',$_POST,$this->file_name)){
                            self::message("Coppy Ok",'alert-success');
                            return true;
                        }

                    }

            }

        }
    }

    public function set()
    {

    }
    public function removeFile($file){
        if (file_exists($file))
            return unlink($file);
    }

    public function getDump()
    {
        var_dump($_FILES, $_POST);
    }
    public function removeAllFiles($files = [],$option = null){
        if ($option === null)
        foreach ($files as $key => $file){
            if (file_exists($file['path']))
                unlink($file['path']);
        }
        else if ($option['method'] === 'dir')
            array_map('unlink',glob($option['path'].'/*.*'));
    }

    /**
     * @return bool
     */
    private function error()
    {
        $flag = false;
        if (!empty($_FILES))
            foreach ($_FILES as $key => $value) {
                if ($value['error'] !== 0) {
                    if (isset($value['name']))
                        $_SESSION['msg'] .= "get an error for" . $value['name'] . "file\n";
                    else
                        $_SESSION['msg'] .= "get an error\n";

                    $flag = true;
                } else {
                    $_SESSION['ok_msg'] .= "{$value['name']}\n";
                }

            }
        else
            return true;
        return $flag;
    }

    private function getExt($file)
    {
        return pathinfo($file)['extension'];
    }

    private function moveFile($directory = '.')
    {
        $files = [];
        foreach ($_FILES as $key => $value) {
            $time = (string)time();
            $u_name = $time .'.' . $this->getExt($value['name']);
            $files[$time] = [
                'success' => false,
                'path' => '/var/www/html/medovinka_fremote/img/'.$directory.'/',
                'file' => $u_name
            ];
            if (move_uploaded_file($value['tmp_name'],$files[$time]['path'] . $u_name)) {
                $this->file_name[$time.'x'] = $u_name;
                $files[$time]['success'] = true;
            }
        }

    return $files;

    }


    public function getImgInfo($filename,$param){
        list($width, $height, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].'/medovinka_fremote/img/gallery/'.reset($filename));

        switch ($param){
            case "width":
                return $width;
            case "height":
                return $height;
        }

    }


}