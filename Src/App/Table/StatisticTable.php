<?php
/**
 * Created by PhpStorm.
 * User: vladv
 * Date: 12/08/2018
 * Time: 13:34
 */

namespace Src\App\Table;


use DateTime;
use Src\App\App;
use Src\Core\Table\Table;

class StatisticTable extends Table
{

    protected $table = 'sales';
    protected $db;

    public function getSalesQuantity($year = null){
        if ($year === null){
             return $this->query("SELECT COUNT( DISTINCT ( id ) ) AS qtty FROM sales WHERE YEAR(date) = YEAR(NOW())",null,true);

        }
    }

    public function getYearSalesStatistic($y = null){
        $labels = [];
        $data = [];
        if ($y === null){
            $year = date('Y');
        }else{
            $year = $y;
        }


        for ($i = 1; $i <= 12; $i++){

            $data[$i-1] = count(App::getInstance()->getDb()->query(
                "SELECT * FROM sales WHERE MONTH(date) = {$i} AND YEAR(date) ={$year}")
            );
            $labels[$i-1] = DateTime::createFromFormat('!m', $i)->format('F');

        }


        return [
            'labels' => $labels,
            'statistic' => $data
        ];


    }
    public function getTask(){
        return '0';
    }
    public function getStock(){

    }
}