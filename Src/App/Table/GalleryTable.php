<?php
namespace Src\App\Table;

use Src\Core\Table\Table;
class GalleryTable extends Table {
    protected $table = "gallery";
    protected $db;

    public function getAllImages(){
        return $this->query("select * from gallery");
    }
    public function find($id)
    {
        return parent::find($id); // TODO: Change the autogenerated stub
    }

    public function getImageTable(){

    }
//list($width, $height, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].'/medovinka_fremote/img/gallery/'.$post->img);
    public function sort(){
        $container = array();

    }

    public function parse()
    {
        $container = $this->all();
        $w_container = array();
        $h_container = array();

        $result = array();
        $path = $_SERVER['DOCUMENT_ROOT'].'/medovinka_fremote/img/gallery/';
        foreach ($container as $key => $value) {
            list($width, $height, $type, $attr) = getimagesize($path.$value->img);
            if ($width > $height)
                $w_container[] = $container[$key];
            else
                $h_container[] = $container[$key];
        }
        foreach ($h_container as $item) {
            $result[] = $item;
        }
        foreach ($w_container as $item) {
            $result[] = $item;
       }


        return $result;
    }

    public function showImage($img){
        $file = "/var/www/html/medovinka_fremote/img/gallery/".$img;
        $files = "/../medovinka_fremote/img/gallery/".$img;
         return file_exists($file)?$files:"https://via.placeholder.com/350x151";
    }


}
