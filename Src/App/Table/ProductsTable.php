<?php


namespace Src\App\Table;
use Src\App\App;
use Src\Core\Table\Table;

class ProductsTable extends Table
{
    protected $table = "merchandise";
    protected $db;
    public function find($id)
    {
        return $this->query("
        SELECT sales.id, sales.customer, sales.quantity, sales.date, sales.price, sales.product, customers.name as customer
        FROM sales
        LEFT JOIN customers ON customer = customers.customer_id
        WHERE sales.id = ?
        ",[$id],true);
    }
    public function allTable(){
        return $this->query("
        SELECT * 
        FROM merchandise
		LEFT JOIN product_category ON merchandise.product_id = product_category.product_id
        
        ");
    }

    public function getMerchandiseId($name){
        return $this->query("
          select merchandise.merchandise_id
          from merchandise 
          where merchandise.name = '{$name}'",null,true);

    }
    public function deleteFromBd($id,$filePath){
        $file = App::getInstance()->getDb()->query("select `merchandise`.`img` from merchandise where merchandise.merchandise_id = {$id}",null,true);
        Upload::getInstance()->removeFile($filePath.'/'.$file->img);
    }
    public function getProductOfId($id){
        return $this->query("select name from merchandise where merchandise_id = {$id}",null,true);
    }
    public function getCategoryOfId($id){
        return $this->query("select product from product_category where product_id = {$id}",null,true);
    }
    public function update($table,$data = [],$option = []){
        $myData = $data;
        $myData['available'] = ($myData['available'] == 'true')?1:0;
        if (!empty($option)){
            foreach ($option as $key => $value)
                $myData['img'] = $value;
        }
        $myData['product_id']= current(App::getInstance()->getTable('categories')->getCategoryId($_POST['product_id']))->product_id;
        if (!empty($data)){
            $query = "update {$table} set ";
            foreach ($myData as $key => $value){
                $myData[$key] = "'{$value}'";
            }

            foreach ($myData as $key => $value){
                $query .= " `{$key}` = {$value},";
            }

            $query = substr($query,0,-1);
            $query .= " where `merchandise`.`merchandise_id` = ".$_GET['id'];
             return $this->db->execute($query);
//        var_dump($query);
        }
    }
    public function getMerchandise(){
        return $this->query("
        select merchandise.name from merchandise");
    }
    public function findEdit($id){
        return $this->query("
        SELECT * 
        FROM merchandise
		LEFT JOIN product_category ON merchandise.product_id = product_category.product_id
        where merchandise.merchandise_id = ?
        ",[$id],true);
    }

    public function getCategories(){
        return $this->query(
            "select * from product_category",null,false
        );
    }

}