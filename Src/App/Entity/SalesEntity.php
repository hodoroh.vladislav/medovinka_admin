<?php

namespace Src\App\Entity;

use Src\App\App;

use Src\Core\Entity\Entity;

class SalesEntity extends Entity
{
    private $productName;
    public function getTest()
    {
        return "test";
    }
    public function getProductName($product_id)
    {
        if ($this->productName === null)
                $this->productName = App::getInstance()->getDb()->query('SELECT merchandise.merchandise_id, merchandise.name AS merchandise FROM merchandise ');
        return $this->productName[$product_id-1]->merchandise;
    }
    public function getCustomerName($customer_id)
    {
        if ($this->productName === null)
                $this->customersName = App::getInstance()->getDb()->query('SELECT customers.customer_id, customers.name AS customer FROM customers ');
        return $this->customersName[$customer_id-1]->customer ;
    }

}