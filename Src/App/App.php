<?php

namespace Src\App;

use Src\Core\Database\MySQL_Database;
use Src\Core\Config;

class App{
    private static $_instance;
    private $db_instance;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new App();
        }
        return self::$_instance;
    }
    public  function getTable($name)
    {

        $class_name = 'Src\\App\\Table\\'.ucfirst($name) . 'Table';
        return new $class_name($this->getDb());
    }

    /**
     * @param null $array : Params for DB creation
     * @return MySQL_Database
     */
    public function getDb($array = null)
    {
        if ($array === null){
            $config = Config\Config::getInstance(ROOT.'/Src/config/config.php');
            if (is_null($this->db_instance))
                $this->db_instance = new MySQL_Database($config->get('db_name'),$config->get('db_user'),$config->get('db_pass'),$config->get('db_host'));
            return $this->db_instance;
        }
        else
        {
            return  new MySQL_Database($array['db_name'],$array['db_user'],$array['db_pass'],$array['db_host']);
        }

    }

    public function forbidden()
    {
        header('HTTP/1.0 403 forbidden');
        die('Acces Interdit');
    }

    public function notFound()
    {
        header('HTTP/1.0 404 Not Found');
        die('Page Introuvable');
    }

    public static function load()
    {
        session_start();
    }
    public function setSession($options = []){
        foreach ($options as $key => $value){
            $_SESSION[$key] = $value;
        }
    }

    public function setSidebar($value)
    {
        $_SESSION['sidebar'] = $value;
    }

    public function getPath($page)
    {
        $path = ROOT.'/pages/admin/pages/';
        if ($page === 'index' || $page === 'login') {
            return $path . 'index.php';
        }

        $page_arr = [];
        preg_match_all('/[a-zA-Z_].[a-zA-Z-_]*/', $page, $page_arr);

        if ($page_arr !== false){
            $page_arr = current($page_arr);
            foreach ($page_arr as $value){
                $path .= $value.'/';
            }
            $path = substr($path,0,-1);
//            var_dump($path);
            return $path.'.php';
        }

        return $path.'index.php';

    }


}