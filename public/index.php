
<?php
define('ROOT',dirname(__DIR__));

require ROOT.'/vendor/autoload.php';

use Src\App\App;
use Src\Core\Html\HTML;
use Src\Core\Statistics\Stats;



App::load();
$nav = HTML::getNav();
$page = isset($_GET["page"])?$_GET["page"]:"home";



Stats::getInstance()->run();



ob_start();

if ($page === 'home')
    require ROOT . '/pages/posts/home.php';




$content = ob_get_clean();

require ROOT . '/pages/templates/default.php';?>

