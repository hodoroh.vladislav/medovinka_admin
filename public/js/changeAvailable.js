$('button.available-btn').click(function () {
    let button = $(this);
    $.ajax({
        type: 'POST',
        url: 'scripts/php/changeAvailable.php',
        data: {'id':button.attr('id'),'value':!button.hasClass('btn-danger')},
        success: function (html) {
            button.toggleClass('btn-danger');

        }
    });

});


