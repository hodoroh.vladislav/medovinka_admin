class Chart{
    constructor(){
        this.url = 'dbchart.php';
        this.dataType = 'json';
        this.method = 'POST';
        this.data = [];
        this.time = "day";
        // this.chartStartColor = Highcharts.Color(Highcharts.getOptions().colors[2]).setOpacity(0.1).get('rgba');

        // this.chartGetData();
        // this.draw(this.data)
        // this.start();
        this.test();
    }
    test(){
        $.when(
            this.chartGetData({sensor:"sensor1"},this.getTimeObject())
        ).done(function (a1) {
            console.log(a1);
        })
    }
    setTime(time){
        this.time = time;
    }
    getTimeObject(){
        return {time:this.time};
    }
    // start(){
    //     $.when(
    //         this.chartGetData({sensor:"sensor1"},this.getTimeObject()),
    //         this.chartGetData({sensor:"sensor2"},this.getTimeObject()),
    //         this.chartGetData({sensor:"sensor3"},this.getTimeObject()),
    //         this.chartGetData({sensor:"sensor4"},this.getTimeObject()),
    //         this.chartGetData({sensor:"sensor5"},this.getTimeObject())
    //     ).done(function (a1,a2,a3,a4,a5) {
    //         chart.draw(
    //             Chart.parseChart(a1[0]),
    //             Chart.parseChart(a2[0]),
    //             Chart.parseChart(a3[0]),
    //             Chart.parseChart(a4[0]),
    //             Chart.parseChart(a5[0])
    //         );
    //     })
    // }
    static parseChart(data){
        let tmp = [];
        $.each(data, function(i, val) {
            let arr = [new Date(val['date']).getTime(),parseFloat(val['value'])];
            tmp.push(arr);
        });

        return tmp;


        // new Date(data[0].date).getTime()
    }


    draw(data1,data2,data3,data4,data5){
        // create the chart
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });
        Highcharts.stockChart('chart_place', {


            title: {
                text: 'Chart Temp'
            },
            // useUTC:false,
            subtitle: {
                text: 'Using ordinal X axis'
            },

            // xAxis: {
            //     gapGridLineWidth: 0
            // },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    second: '<br/>%H:%M:%S',
                    minute: '<br/>%H:%M:%S',
                    hour: '<br/>%H:%M:%S',
                    day: '%Y<br/>%b-%d',
                    week: '%Y<br/>%b-%d',
                    month: '%Y-%b',
                    year: '%Y'
                },
                minTickInterval: 60000
            },

            rangeSelector: {
                buttons: [{
                    type: 'hour',
                    count: 1,
                    text: '1h'
                }, {
                    type: 'day',
                    count: 1,
                    text: '1D'
                }, {
                    type: 'all',
                    count: 1,
                    text: 'All'
                }],
                selected: 1,
                inputEnabled: true
            },

            series: [
                {
                    name: 'Serre',
                    type: 'area',
                    data: data1,
                    color:'#FFC107',
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, this.chartStartColor],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                },
                {
                    name: 'le haut de la serre',
                    color:'#FF5722',
                    type: 'area',
                    data: data2,
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    // color: '#FF0000',
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, this.chartStartColor],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                },
                {
                    name: 'Le sol de la serre',
                    type: 'area',
                    color:'#795548',
                    data: data3,
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    // color: '#00FF00',
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, this.chartStartColor],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                },
                {
                    name: 'Box',
                    type: 'area',
                    color:'#607D8B',
                    data: data4,
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, this.chartStartColor],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                },
                {
                    name: 'Dehors',
                    type: 'area',
                    color:'#4CAF50',
                    data: data5,
                    gapSize: 2,
                    tooltip: {
                        valueDecimals: 2
                    },
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, this.chartStartColor],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    threshold: false
                }
            ]
        });
    }

    chartGetData(data,time){
        let tmp = [];
        return $.ajax({
            url: this.url,
            data:Object.assign(data,time),
            dataType : this.dataType,
            method: this.method,
            success:
                function (data, textStatus) {
                    // $.each(data, function(i, val) {
                    //     // chart.parseChart(i,val,tdata);
                    //     let arr = [new Date(val['date']).getTime(),parseFloat(val['value'])];
                    //     // chart.data.push(arr);
                    //     tmp.push(arr);
                    // });
                    console.log(data);
                }




        });
        // this.draw(this.data);
    }
}
let chart = new Chart();
// chart.draw(chart.data);
// console.log(chart.data);
// chart.draw();
function hc(data) {

    // create the chart
    Highcharts.stockChart('container', {


        title: {
            text: 'Chart Temp'
        },

        subtitle: {
            text: 'Using ordinal X axis'
        },

        xAxis: {
            gapGridLineWidth: 0
        },

        rangeSelector: {
            buttons: [{
                type: 'hour',
                count: 1,
                text: '1h'
            }, {
                type: 'day',
                count: 1,
                text: '1D'
            }, {
                type: 'all',
                count: 1,
                text: 'All'
            }],
            selected: 1,
            inputEnabled: false
        },

        series: [{
            name: 'AAPL',
            type: 'area',
            data: data,
            gapSize: 2,
            tooltip: {
                valueDecimals: 2
            },
            fillColor: {
                linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops: [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
            },
            threshold: false
        }]
    });
}


$(".day").on("click",function () {
    chart.setTime("day");
    chart.start();

});

$(".week").on("click",function () {
    chart.setTime("week");
    chart.start();

});

$(".month").on("click",function () {
    chart.setTime("month");
    chart.start();

});

$(".alltime").on("click",() => {
    chart.setTime("alltime");
    chart.start();
});


// $.getJSON('https://cdn.rawgit.com/highcharts/highcharts/057b672172ccc6c08fe7dbb27fc17ebca3f5b770/samples/data/new-intraday.json', function (data) {
//     console.log(data);
// })