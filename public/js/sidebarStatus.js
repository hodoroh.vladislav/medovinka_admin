class SidebarStatus {
    constructor(sidebar){
        this.sidebar = sidebar;
    }
    getToggle(){
        return this.sidebar.hasClass('toggled');
    }
    test(){
        self = this;
        $('#sidebarToggle').click(function () {
            console.log(self.getToggle());
        })
    }
    toggled(){
        self = this;
       $.ajax({
           type: 'POST',
           url: 'scripts/php/setSidebar.php',
           data: 'sidebar=' + this.getToggle(),
           success: function (html) {
               console.log(html);
           }
       })
    }
}

