class FormValidation{
    
    constructor(form){
        this.valid = false;
        this.form = form[0];
        this.available = !$('#available_btn').hasClass('btn-danger');
        this.availableMonitoring();
    }

    run(){
        if (this.validate()){
            this.misc();

            return true;
        }
        return false;
}
availableMonitoring(){
        self = this;
    $('#available_btn').click(function () {
        $(this).toggleClass('btn-danger');
        self.available = !self.available;

    });
}


    showError(){
        $('<div class="alert alert-danger">Заполните форму</div>').insertAfter($('.breadcrumb'));
    }
    showWarning(){
        $('<div class="alert alert-warning">Некоторые поля не заполнены</div>').insertAfter($('.breadcrumb'));
    }
    resetError(){
        $('div.alert').remove();
    }
    getParam(sVar){
        return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }
    check(){
        let self = this;
        self.resetError();
        let flag = false;
        let form = this.form;
        let badArray = [];

        $.each(form , function (index,value) {

            if (value.type === 'file' && (self.getParam('page').localeCompare('merchandise.merchandise_add') === 0 ) ){
                if (value.value === '' && (value.checked === false || value.checked === true) ){
                    if (!flag)
                        flag = true;
                    badArray.push(value);
                }else{
                    $(value).parent().removeClass('text-danger');
                }
            }else
            {
                if (value.value === '' && (value.checked === false || value.checked === true) ){
                    if (!flag)
                        flag = true;
                    badArray.push(value);
                }else{
                    $(value).parent().removeClass('text-danger');
                }
            }

        });

        if (flag)
            self.showError();
        else{
            self.resetError();
            self.valid = true;
        }

        return badArray;
    }

    validate(){

        $.each(this.form,function (index,value) {
            $(value).parent().removeClass('text-danger');
        });
        $.each(this.check(),function (index,value) {
            $(value).parent().addClass('text-danger');
        });
        return this.valid;

    }


    misc(){
        $('<input />').attr('type','hiden')
            .attr('name','available')
            .attr('value',this.available)
            .appendTo(this.form);
    }

}

// $(document).ready( function ()
//
// {


    // $('#available_btn').click(function () {
    //     $(this).toggleClass('btn-danger');
    //     form_add.available = !form_add.available;
    // });

     getForm = function(form){
        return new FormValidation(form);
    };






// }) ;