<?php

use Src\App\App;
use Src\Core\Auth\DBAuth;

define('ROOT',dirname(__DIR__));
require ROOT.'/vendor/autoload.php';
$app = App::getInstance();
$auth = new DBAuth($app->getDb());
App::load();

//Auth
if ( !$auth->logged()) {
    header('Location: login.php') ;
}
if (!isset($_SESSION['sidebar']))
    App::getInstance()->setSession(['sidebar' => true]);








//$nav = HTML::getNav();
if(isset($_GET['page'])) {
    $page = $_GET['page'];
}
else {
    $page = 'index';
}

if ($page === 'logout'){
    $auth->logout($_SESSION);
}



//$logout = HTML::getLogout();

//$adminNav = HTML::getAdminNav();
//$adminSideBar = HTML::getSideBar($page);


ob_start();

require App::getInstance()->getPath($page);


$content = ob_get_clean();

require ROOT.'/pages/templates/adminDefault.php';