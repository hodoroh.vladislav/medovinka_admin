<?php
use Src\App\App;

define('SERVER',$_SERVER['DOCUMENT_ROOT']);
require SERVER.'/composer_medovinka/vendor/autoload.php';

App::load();

if ($_POST['sidebar'] ===  'true')
    App::getInstance()->setSidebar(true);
else
    App::getInstance()->setSidebar(false);

